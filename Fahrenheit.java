package com.pppt;

public class Fahrenheit extends Suhu{
    public Fahrenheit(double temp){
        super(temp);
    }

    public void fah(double temp){
        r = (temp-32)*4/9;
        c = (temp-32)*5/9;
        k = (temp-32)*5/9+273.15;
        System.out.println("Fahrenheit  : "+temp);
        System.out.println("Celcius     : "+c);
        System.out.println("Kelvin      : "+k);
        System.out.println("Reamur      : "+r);
    }

    @Override
    public String temperatur() {
        return "Fahrenheit";
    }
}
