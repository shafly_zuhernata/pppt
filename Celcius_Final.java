package com.pppt;

public class Celcius extends Suhu{
    public Celcius(double temp){
        super(temp);
    }

    public void cel(double temp){
        r = temp*4/5;
        f = (temp*9/5)+32;
        k = temp+273.15;
        System.out.println("Celcius     : "+temp);
        System.out.println("Fahrenheit  : "+f);
        System.out.println("Kelvin      : "+k);
        System.out.println("Reamur      : "+r);
    }

    @Override
    public String temperatur() {
        return "Celcius";
    }
}
