package com.pppt;

public class Kelvin extends Suhu{
    public Kelvin(double temp){
        super(temp);
    }

    public void kel(double temp){
        r = temp-217.72;
        c = temp-273.15;
        f = temp*9/5-459.67;
        System.out.println("Kelvin      : "+temp);
        System.out.println("Celcius     : "+c);
        System.out.println("Fahrenheit  : "+f);
        System.out.println("Reamur      : "+r);
    }

    @Override
    public String temperatur() {
        return "Kelvin";
    }
}
