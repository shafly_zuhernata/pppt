package com.pppt;

public class Reamur extends Suhu{
    public Reamur(double temp){
        super(temp);
    }

    public void rea(double temp){
        k = (temp/0.8)+273.15;
        c = temp/0.8;
        f = (temp*2.25)+32;
        System.out.println("Reamur      : "+temp);
        System.out.println("Celcius     : "+c);
        System.out.println("Fahrenheit  : "+f);
        System.out.println("Kelvin      : "+k);
    }

    @Override
    public String temperatur() {
        return "Reamur";
    }
}
