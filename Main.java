package com.pppt;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Program Pendeteksi & Pengkonversi Temperatur");
        System.out.println("Menu Skala");
        System.out.println("1. Celcius\n2. Fahrenheit\n3. Kelvin\n4. Reamur");
        System.out.print("Pilih Menu : ");
        Integer m = input.nextInt();

        switch (m){
            case 1:
                System.out.print("Masukkan Angka Celcius : ");
                double c = input.nextDouble();
                Celcius celc = new Celcius(c);
                celc.cel(c);
                break;
            case 2:
                System.out.print("Masukkan Agka Fahrenheit : ");
                double f = input.nextDouble();
                Fahrenheit fahr = new Fahrenheit(f);
                fahr.fah(f);
                break;
            case 3:
                System.out.print("Masukkan Angka Kelvin : ");
                double k = input.nextDouble();
                Kelvin kelv = new Kelvin(k);
                kelv.kel(k);
                break;
            case 4:
                System.out.print("Masukkan Angka Reamur : ");
                double r = input.nextDouble();
                Reamur ream = new Reamur(r);
                ream.rea(r);
                break;
            default:
                System.out.println("Masukkan Angka Dengan Benar! (1-4)");
        }
    }
}
